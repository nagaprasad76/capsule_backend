﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using TaskManager.Entities;


namespace TaskManager.DataLayer
{
    public class TaskManagerDbContext : DbContext
    {
        public TaskManagerDbContext() : base("dbconnection")
        {
           Database.SetInitializer(new MigrateDatabaseToLatestVersion<TaskManagerDbContext, TaskManager.DataLayer.Migrations.Configuration>());
        }

        public DbSet<Task> Tasks { get; set; }

    }
}
