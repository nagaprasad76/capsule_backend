namespace TaskManager.DataLayer.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using TaskManager.Entities;

    internal sealed class Configuration : DbMigrationsConfiguration<TaskManager.DataLayer.TaskManagerDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "TaskManager.DataLayer.TaskManagerDbContext";
        }

        protected override void Seed(TaskManager.DataLayer.TaskManagerDbContext context)
        {
            if (!context.Database.Exists())
                context.Tasks.
                   AddOrUpdate(new Task() { TaskName = "Coding", Priority = 25, StartDate = DateTime.Now, EndDate = DateTime.Now, ParentTask = null, TaskStatus = true },
                   new Task() { TaskName = "Reading", Priority = 25, StartDate = DateTime.Now, EndDate = DateTime.Now, ParentTask = null, TaskStatus = true });
        }
    }
}
