﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using TaskManager.DataLayer;
using TaskManager.Entities;

namespace TaskManager.BusinessLayer
{
    public class TaskManagerBusiness
    {
        public List<Task> GetAllTasks()
        {
            using (TaskManagerDbContext dbcontext = new TaskManagerDbContext())
            {
                return dbcontext.Tasks.ToList();
            }
        }

        public Task GetTaskById(long taskId)
        {
            using (TaskManagerDbContext dbcontext = new TaskManagerDbContext())
            {
                return dbcontext.Tasks.Find(taskId);
            }
        }

        public Task GetByTaskName(string taskName)
        {
            using (TaskManagerDbContext dbcontext = new TaskManagerDbContext())
            {
                return dbcontext.Tasks.SingleOrDefault(x => x.TaskName.ToUpper() == taskName.ToUpper());
            }
        }

        public void AddTask(Task newTask)
        {
            using (TaskManagerDbContext dbcontext = new TaskManagerDbContext())
            {
                if (!TaskExists(newTask.TaskID))
                {
                    dbcontext.Tasks.Add(newTask);
                    dbcontext.SaveChanges();
                }
                return;
            }
        }

        private bool TaskExists(long taskId)
        {
            using (TaskManagerDbContext dbcontext = new TaskManagerDbContext())
            {
                return dbcontext.Tasks.Count(e => e.TaskID == taskId) > 0;
            }
        }

        public void UpdateTask(Task updatedTask)
        {
            using (TaskManagerDbContext dbcontext = new TaskManagerDbContext())
            {
                if (TaskExists(updatedTask.TaskID))
                {
                    Task existingTask = dbcontext.Tasks.Where(temp => temp.TaskID == updatedTask.TaskID).FirstOrDefault();
                    existingTask.TaskName = updatedTask.TaskName;
                    existingTask.Priority = updatedTask.Priority;
                    existingTask.StartDate = updatedTask.StartDate;
                    existingTask.EndDate = updatedTask.EndDate;
                    existingTask.TaskStatus = updatedTask.TaskStatus;
                    existingTask.ParentTaskID = updatedTask.ParentTaskID;
                    dbcontext.SaveChanges();
                }
                return;
            }
        }

        public bool DeleteTask(long taskID)
        {
            using (TaskManagerDbContext dbcontext = new TaskManagerDbContext())
            {
                Task childTask = dbcontext.Tasks.Where(temp => temp.ParentTaskID == taskID).FirstOrDefault();

                if (childTask == null)
                {
                    Task taskToRemove = dbcontext.Tasks.Where(temp => temp.TaskID == taskID).FirstOrDefault();
                    dbcontext.Tasks.Remove(taskToRemove);
                    dbcontext.SaveChanges();
                    return true;
                }
                else
                    return false;

            }
        }

        public List<Task> EndTask(long taskId)
        {
            using (TaskManagerDbContext dbcontext = new TaskManagerDbContext())
            {
                var context = dbcontext.Tasks.Find(taskId);
                context.EndDate = DateTime.Now;
                context.TaskStatus = false;

                dbcontext.SaveChanges();

                return dbcontext.Tasks.ToList();
            }
        }
    }
}
