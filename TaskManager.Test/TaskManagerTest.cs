﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManager.Entities;
using TaskManager.API.ApiControllers;
using TaskManager.API;
using System.Web.Http;
using System.Web.Http.Results;
using System.Net;
using TaskManager.API.Models;

namespace TaskManager.Test
{
    [TestFixture]
    class TaskManagerTest
    {
        TasksController _taskObj;
        long taskId = 0;

        [SetUp]
        public void Setup()
        {
            _taskObj = new TasksController();
        }

        [Test]
        [Order(1)]
        public void TestWebApiGetAll()
        {
            List<TaskDetail> tasks = _taskObj.Get();
            Assert.Greater(tasks.Count(), 0);
        }
        [Test]
        [Order(2)]
        public void TestWebApiAddTask()
        {
            // Arrange
            Task item = new Task();
            item.TaskName = "TaskApiTest1";
            item.ParentTaskID = 1;
            item.Priority = 15;
            item.StartDate = DateTime.Now;
            item.EndDate = DateTime.Now;
            item.TaskStatus = true;

            //Act
            IHttpActionResult actionResult = _taskObj.Post(item);
            Task test = _taskObj.GetByTaskName("TaskApiTest1");
            taskId = test.TaskID;

            //Assert
            Assert.IsInstanceOf(typeof(OkResult), actionResult);
        }
        [Test]
        [Order(3)]
        public void TestWebAPIGetByTaskId()
        {
            IHttpActionResult actionResult = _taskObj.Get(taskId);
            var contentResult = actionResult as OkNegotiatedContentResult<Task>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(taskId, contentResult.Content.TaskID);
        }

        [Test]
        [Order(4)]
        public void TestGetByIdReturnsNotFound()
        {
            // Act
            IHttpActionResult actionResult = _taskObj.Get(2000);

            // Assert
            Assert.IsInstanceOf(typeof(NotFoundResult), actionResult);
        }
        [Test]
        [Order(5)]
        public void TestWebApiUpdateTask()
        {
            Task item = new Task();
            item.TaskID = taskId;
            item.TaskName = "TaskApiTest1";
            item.Priority = 16;
            item.ParentTaskID = 1;
            item.StartDate = DateTime.Now;
            item.EndDate = DateTime.Now.AddDays(30);
            item.TaskStatus = true;

            IHttpActionResult actionResult = _taskObj.Put(item);
            var contentResult = actionResult as NegotiatedContentResult<Task>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.AreEqual(HttpStatusCode.Accepted, contentResult.StatusCode);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(16, contentResult.Content.Priority);
        }
        [Test]
        [Order(6)]
        public void TestWebApiEndTask()
        {
            // Arrange
            Task item = new Task();
            item.TaskID = taskId;
            item.TaskName = "TaskApiTest1";
            item.Priority = 10;
            item.StartDate = DateTime.Now;
            item.EndDate = DateTime.Now;
            item.TaskStatus = false;

            //Act
            IHttpActionResult actionResult = _taskObj.End(item);
            Task itemAfterEnd = _taskObj.GetByTaskName("TaskApiTest1");

            //Assert
            //  Assert.IsInstanceOf(typeof(OkResult), actionResult);
            Assert.AreEqual(false, itemAfterEnd.TaskStatus);
        }

        [Test]
        [Order(7)]
        public void TestDeleteBadRequest()
        {
            //Act
            IHttpActionResult actionResult = _taskObj.Delete(0);

            //Assert
            Assert.IsInstanceOf(typeof(BadRequestErrorMessageResult), actionResult);
        }
       
    }
}
