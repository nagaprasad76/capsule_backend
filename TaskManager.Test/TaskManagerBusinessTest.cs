﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManager.Entities;
using TaskManager.BusinessLayer;
using TaskManager.API;
using System.Web.Http;
using System.Web.Http.Results;
using System.Net;
using TaskManager.API.Models;
using NUnit.Framework;

namespace TaskManager.Test
{
    class TaskManagerBusinessTest
    {
        TaskManagerBusiness _taskObj;
        long taskId = 0;

        [SetUp]
        public void Setup()
        {
            _taskObj = new TaskManagerBusiness();
        }

        [Test]
        [Order(1)]
        public void TestGetAll()
        {
            int actual = _taskObj.GetAllTasks().Count;
            Assert.Greater(actual, 0);
        }
        [Test]
        [Order(2)]
        public void TestAddTask()
        {
            Task item = new Task();
            item.TaskID = 20;
            item.TaskName = "TestBusiness1";
            item.ParentTaskID = 1;
            item.Priority = 15;
            item.StartDate = DateTime.Now;
            item.EndDate = DateTime.Now;
            item.TaskStatus = true;
            _taskObj.AddTask(item);

            Task test = _taskObj.GetByTaskName("TestBusiness1");
            taskId = test.TaskID;
            Assert.AreEqual("TestBusiness1", test.TaskName);
        }
        [Test]
        [Order(3)]
        public void TestGetByTaskId()
        {
            Task item = _taskObj.GetTaskById(taskId);
            Assert.AreEqual(taskId, item.TaskID);
        }

        [Test]
        [Order(4)]
        public void TestUpdateTask()
        {
            Task item = new Task();
            item.TaskID = taskId;
            item.TaskName = "TestBusiness1";
            item.Priority = 20;
            item.StartDate = DateTime.Now;
            item.EndDate = DateTime.Now.AddDays(10);
            item.TaskStatus = true;

            _taskObj.UpdateTask(item);
            Task itemafterupdate = _taskObj.GetByTaskName("TestBusiness1");
            Assert.AreEqual(20, itemafterupdate.Priority);

        }
        [Test]
        [Order(5)]
        public void TestEndTask()
        {
            _taskObj.EndTask(taskId);

            Task itemafterupdate = _taskObj.GetByTaskName("TestBusiness1");
            Assert.AreEqual(false, itemafterupdate.TaskStatus);
        }
        [Test]
        [Order(6)]
        public void TestDeleteTask()
        {
            _taskObj.DeleteTask(taskId);

            Task item = _taskObj.GetTaskById(taskId);
            Assert.AreEqual(null, item);
        }
    }

}

