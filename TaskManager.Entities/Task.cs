﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace TaskManager.Entities
{
    [Table("Tasks")]
    public class Task
    {
        [Key]
        public long TaskID { get; set; }
        public string TaskName { get; set; }
        public Nullable<int> Priority { get; set; }

        [Column(TypeName = "date")]
        public Nullable<DateTime> StartDate { get; set; }

        [Column(TypeName = "date")]
        public Nullable<DateTime> EndDate { get; set; }

        public Nullable<bool> TaskStatus { get; set; }

        public long? ParentTaskID { get; set; }
        [ForeignKey("ParentTaskID")]
        public virtual Task ParentTask { get; set; }

    }
}
