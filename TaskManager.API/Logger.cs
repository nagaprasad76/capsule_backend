﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;

namespace TaskManager.API
{
    public class Logger
    {
        public static void WriteLog(string errorMessage)
        {
            string currentDir = HttpRuntime.AppDomainAppPath;
            DirectoryInfo directory = new DirectoryInfo(currentDir);
            string logFilePath = currentDir + "logs\\ProgramLog" + "-" + DateTime.Today.ToString("yyyyMMdd") + "." + "txt";

            if (logFilePath.Equals("")) return;

            #region Create the Log file directory if it does not exists
            DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo = new FileInfo(logFilePath);
            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            #endregion Create the Log file directory if it does not exists

            var content = "Error Message: " + "Date: " + DateTime.Now + Environment.NewLine
                           + errorMessage + Environment.NewLine;
            using (StreamWriter sw = new StreamWriter(logFilePath, true))
            {
                sw.WriteLine(content);
            }
        }
    }
}