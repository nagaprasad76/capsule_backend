﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using TaskManager.API.CustomFilter;
using System.Net.Http.Formatting;

namespace TaskManager.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
             config.Filters.Add(new CustomExceptionFilter());
            // Web API configuration and services
            config.EnableCors();

            // configure json formatter
            JsonMediaTypeFormatter jsonFormatter = new JsonMediaTypeFormatter();
            config.Formatters.Add(jsonFormatter);

            // Web API routes
            config.MapHttpAttributeRoutes();            
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
