﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskManager.API.Models
{
    public class TaskDetail
    {
        public long TaskID { get; set; }
        public string TaskName { get; set; }
        public Nullable<int> Priority { get; set; }
        public Nullable<DateTime> StartDate { get; set; }
        public Nullable<DateTime> EndDate { get; set; }
        public Nullable<bool> TaskStatus { get; set; }

        public long? ParentTaskID { get; set; }
        public string ParentTaskName { get; set; }

    }
}