﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskManager.Entities;
using TaskManager.DataLayer;
using System.Web.Http.Cors;
using TaskManager.BusinessLayer;
using TaskManager.API.CustomFilter;
using TaskManager.API.Models;

namespace TaskManager.API.ApiControllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TasksController : ApiController
    {
        TaskManagerBusiness businessObj = new TaskManagerBusiness();

        [HttpGet, Route("api/GetAll")]
        [CustomExceptionFilter]
        public List<TaskDetail> Get()//Get All Tasks
        {
            var response = businessObj.GetAllTasks();
            return BuildTaskList(response);
        }

        [HttpGet, Route("api/Get/{id}")]
        [CustomExceptionFilter]
        public IHttpActionResult Get(long id) //Get Task By ID
        {
            Task task = businessObj.GetTaskById(id);
            if (task == null)
            {
                return NotFound();
            }
            TaskDetail taskDetail = new TaskDetail
            {
                TaskID = task.TaskID,
                TaskName = task.TaskName,
                ParentTaskID = task.ParentTaskID
            };
            if (taskDetail.ParentTaskID != null)
            {
                var resp = businessObj.GetAllTasks().FirstOrDefault(t => t.TaskID == task.ParentTaskID);
                taskDetail.ParentTaskName = resp != null ? resp.TaskName : null;
            }
            taskDetail.Priority = task.Priority;
            taskDetail.StartDate = task.StartDate;
            taskDetail.EndDate = task.EndDate;
            taskDetail.TaskStatus = task.TaskStatus;
            return Ok(taskDetail);
        }

        [HttpPost, Route("api/Add")]
        [CustomExceptionFilter]
        public IHttpActionResult Post(Task newTask)// Add new Task
        {
            newTask.TaskStatus = true;
            businessObj.AddTask(newTask);
            return Ok();
        }

        [HttpPut, Route("api/Update")]
        [CustomExceptionFilter]
        public IHttpActionResult Put(Task updatedTask)// Update existing task
        {
            businessObj.UpdateTask(updatedTask);
            return Content(HttpStatusCode.Accepted, updatedTask);
        }

        [HttpDelete, Route("api/Delete/{id}")]
        [CustomExceptionFilter]
        public IHttpActionResult Delete(long id)// Delete the existing task
        {
            if (id <= 0)
                return BadRequest("Not a valid ID");
            if (!businessObj.DeleteTask(id))
                return Content(HttpStatusCode.BadRequest, "Cannot delete this dependent task");
            return Ok(HttpStatusCode.OK);
        }

        [HttpPut, Route("api/End")]
        [CustomExceptionFilter]
        public IHttpActionResult End(Task task)// End the active task
        {
            var response = businessObj.EndTask(task.TaskID);
            if (response.Count == 0)
            {
                return NotFound();
            }

            return Ok(BuildTaskList(response));
        }


        [Route("api/GetByTaskName/{taskname}")]
        [HttpGet]
        public Task GetByTaskName(string taskName)
        {
            return businessObj.GetByTaskName(taskName);

        }

        private List<TaskDetail> BuildTaskList(List<Task> taskResp)
        {
            List<TaskDetail> tasks = new List<TaskDetail>();

            try
            {
                foreach (var task in taskResp)
                {
                    TaskDetail taskDetail = new TaskDetail
                    {
                        TaskID = task.TaskID,
                        TaskName = task.TaskName,
                        ParentTaskID = task.ParentTaskID
                    };
                    if (taskDetail.ParentTaskID != null)
                    {
                        var resp = taskResp.FirstOrDefault(t => t.TaskID == task.ParentTaskID);
                        taskDetail.ParentTaskName = resp != null ? resp.TaskName : null;
                    }
                    taskDetail.Priority = task.Priority;
                    taskDetail.StartDate = task.StartDate;
                    taskDetail.EndDate = task.EndDate;
                    taskDetail.TaskStatus = task.TaskStatus;

                    tasks.Add(taskDetail);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.Message);
            }

            return tasks;
        }

    }
}
